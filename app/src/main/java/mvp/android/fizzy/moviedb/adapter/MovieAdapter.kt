package mvp.android.fizzy.moviedb.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import mvp.android.fizzy.moviedb.R
import mvp.android.fizzy.moviedb.model.Movie
import android.support.v7.widget.RecyclerView
import android.view.View
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.movie_list.view.*

class MovieAdapter(private var context: Context, private var list: List<Movie>) : RecyclerView.Adapter<MovieAdapter.ViewHolder>() {

    val BASE_URL = "https://image.tmdb.org/t/p/w500"

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.movieTitle.text = list[position].title
        holder.movieDesc.text = list[position].overview
        Picasso.get().load(BASE_URL + list[position].poster_path).into(holder.movieImage)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(context).inflate(R.layout.movie_list, parent, false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val movieTitle = itemView.movie_title!!
        val movieDesc = itemView.movie_desc!!
        val movieImage =  itemView.movie_image!!
    }
}