package mvp.android.fizzy.moviedb.model

data class MoviesResponse (
    var page: Int? = null,
    var totalResults: Int? = null,
    var totalPages: Int? = null,
    var results: List<Movie>? = null
)