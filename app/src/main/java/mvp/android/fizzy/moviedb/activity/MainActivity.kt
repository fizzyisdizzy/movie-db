package mvp.android.fizzy.moviedb.activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*
import mvp.android.fizzy.moviedb.R
import mvp.android.fizzy.moviedb.adapter.MovieAdapter
import mvp.android.fizzy.moviedb.api.ApiClient
import mvp.android.fizzy.moviedb.api.ApiInterface
import mvp.android.fizzy.moviedb.model.Movie
import mvp.android.fizzy.moviedb.model.MoviesResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity() {

    private val TAG: String = MainActivity::class.java.simpleName
    private val API_KEY: String = "e4f9e61f6ffd66639d33d3dde7e3159b"
    lateinit var myAdapter: MovieAdapter
    private val apiServices = ApiClient.client.create(ApiInterface::class.java)
    private val call = apiServices.getTopRatedMovies(API_KEY)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setupRecyclerView()
        progressBar.visibility = View.VISIBLE

        call.enqueue(object : Callback<MoviesResponse> {
            override fun onResponse(call: Call<MoviesResponse>, response: Response<MoviesResponse>) {
                val listOfMovies: List<Movie> = response.body()?.results!!
                myAdapter = MovieAdapter(applicationContext, listOfMovies)
                recycler_view.adapter = myAdapter
                progressBar.visibility = View.GONE
            }

            override fun onFailure(call: Call<MoviesResponse>?, t: Throwable?) {
                progressBar.visibility = View.GONE
                Log.e(TAG, t.toString())
            }
        })

    }

    private fun setupRecyclerView() {
        recycler_view.layoutManager = LinearLayoutManager(this)
        recycler_view.setHasFixedSize(true)
    }
}

