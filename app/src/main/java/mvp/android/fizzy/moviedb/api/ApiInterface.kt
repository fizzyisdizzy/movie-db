package mvp.android.fizzy.moviedb.api

import mvp.android.fizzy.moviedb.model.MoviesResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by Fizzy on 22/08/2018.
 */
interface ApiInterface {

    @GET("movie/top_rated?")
    fun getTopRatedMovies(@Query("api_key") apiKey: String): Call<MoviesResponse>

}