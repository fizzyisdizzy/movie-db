package mvp.android.fizzy.moviedb.api

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by Fizzy on 22/08/2018.
 */
class ApiClient {

    companion object {
        val BASE_URL: String = "https://api.themoviedb.org/3/"
        var retrofit: Retrofit? = null
        val client: Retrofit
            get() {
                if (retrofit == null) {
                    retrofit = Retrofit.Builder()
                            .baseUrl(BASE_URL)
                            .addConverterFactory(GsonConverterFactory.create())
                            .build()
                }
                return retrofit!!
            }
    }
}